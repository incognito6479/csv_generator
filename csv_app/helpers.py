from .models import Schema, SchemaColumn
import random


def saving_schema_columns(form, self):
    schema_instance = Schema.objects.get(title=self.request.POST.get('title'))
    column_name = self.request.POST.getlist('column_name')
    column_type = self.request.POST.getlist('column_type')
    column_type_from = self.request.POST.getlist('column_type_from')
    column_type_to = self.request.POST.getlist('column_type_to')
    column_order = self.request.POST.getlist('column_order')
    for i in range(0, len(column_name)):
        schema_column = SchemaColumn(schema=schema_instance,
                                     name=column_name[i],
                                     type=column_type[i],
                                     type_info1=column_type_from[i],
                                     type_info2=column_type_to[i],
                                     order=column_order[i]
                                     )
        schema_column.save()
    return form


def str_generate(string_character):
    big_letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    small_letters = 'abcdefghijklmnopqrstuvwxyz'
    result = ""
    for i in range(0, random.randint(9, 26)):
        letter_list = random.choices((big_letters, small_letters))
        result += letter_list[0][i]

    return string_character + result + string_character


def int_generate(r_from, r_to):
    range_1 = random.randint(r_from, r_to)
    result = str(range_1)
    return result


def boolean_generate():
    b_list = ['true', 'false']
    return b_list[random.randint(0, 1)]
