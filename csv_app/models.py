from django.db import models
from datetime import date


COLUMN_SEPARATOR = (
    (',', 'Comma (,)'),
    (';', 'Dot comma (;)'),
)

STRING_CHARACTER = (
    ('"', 'Double-quote (")'),
    ("'", "Single quote (')"),
)


class Schema(models.Model):
    title = models.CharField(max_length=500, unique=True, verbose_name='Title')
    modified = models.DateField(default=date.today(), verbose_name='Modified')
    column_separator = models.CharField(max_length=500, choices=COLUMN_SEPARATOR, default=',',
                                        verbose_name='Column separator')
    string_character = models.CharField(max_length=500, choices=STRING_CHARACTER, default='"',
                                        verbose_name='String character')


COLUMN_TYPE = (
    ('str', 'String'),
    ('int', 'Integer'),
    ('bool', 'Boolean'),
)


class SchemaColumn(models.Model):
    schema = models.ForeignKey(Schema, on_delete=models.CASCADE)
    name = models.CharField(max_length=500, verbose_name='Column name')
    type = models.CharField(max_length=500, choices=COLUMN_TYPE, default='str', verbose_name='Type')
    type_info1 = models.CharField(max_length=200, blank=True, null=True, verbose_name='From')
    type_info2 = models.CharField(max_length=200, blank=True, null=True, verbose_name='To')
    order = models.IntegerField(verbose_name='Order')

