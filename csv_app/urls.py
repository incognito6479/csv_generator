from django.urls import path
from .views import LoginView, SchemaView, LogoutView, SchemaCreateView, SchemaDeleteView, SchemaEditView,\
    SchemaColumnDelete, create_data_sets
from django.contrib.auth.decorators import login_required

urlpatterns = [
    path('', LoginView.as_view(), name='login'),
    path('logout/', login_required(LogoutView.as_view()), name='logout'),
    path('schema/', login_required(SchemaView.as_view()), name='schemas'),
    path('schema/add/', login_required(SchemaCreateView.as_view()), name='schemas-create'),
    path('schema/edit/<int:pk>', login_required(SchemaEditView.as_view()), name='schema-edit'),
    path('schema/column/delete/<int:pk>/<int:id>', login_required(SchemaColumnDelete.as_view()),
         name='schema-column-delete'),
    path('schema/delete/<int:pk>', login_required(SchemaDeleteView.as_view()), name='schemas-delete'),
    path('schema/dataset/create', login_required(create_data_sets), name='data-sets-generate'),
]
