import csv
import os

from django.conf import settings
from django.contrib.auth import login, logout, authenticate
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import View
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView

from .helpers import saving_schema_columns, str_generate, int_generate, boolean_generate
from .models import Schema, SchemaColumn


class LoginView(View):
    def get(self, request):
        if request.user.is_authenticated:
            return redirect('schemas')
        return render(request, 'login.html')

    def post(self, request):
        user = authenticate(username=request.POST.get('login'), password=request.POST.get('password'))
        if user is not None:
            login(request, user)
            return redirect('schemas')
        context = {
            'error': 'Username or password is incorrect',
        }
        return render(request, 'login.html', context)


class LogoutView(View):
    def get(self, request):
        logout(request)
        return redirect('login')


class SchemaView(ListView):
    model = Schema
    template_name = 'index.html'


class SchemaDeleteView(DeleteView):
    model = Schema
    success_url = reverse_lazy('schemas')


class SchemaCreateView(CreateView):
    model = Schema
    fields = ['title', 'column_separator', 'string_character']
    template_name = 'add_schema.html'
    success_url = reverse_lazy('schemas')

    def form_valid(self, form):
        form_ = super().form_valid(form)
        saving_schema_columns(form, self)
        return form_


class SchemaEditView(UpdateView):
    model = Schema
    fields = ['title', 'column_separator', 'string_character']
    template_name = 'edit_schema.html'
    success_url = reverse_lazy('schemas')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        schema_columns = SchemaColumn.objects.filter(schema_id=self.kwargs['pk'])
        context['id'] = self.kwargs['pk']
        context['schema_columns'] = schema_columns
        return context

    def form_valid(self, form):
        form_ = super().form_valid(form)
        saving_schema_columns(form_, self)
        return form_


class SchemaColumnDelete(DeleteView):
    model = SchemaColumn

    def get_success_url(self):
        id_ = self.kwargs['id']
        return reverse_lazy('schema-edit', kwargs={'pk': id_})

    def get(self, *args, **kwargs):
        return self.post(*args, **kwargs)


def create_data_sets(request):
    schema = Schema.objects.get(id=request.POST.get('schema_id'))
    schema_column = SchemaColumn.objects.filter(schema_id=request.POST.get('schema_id')).order_by('order')
    title = schema.title
    file_name = os.path.join(settings.MEDIA_ROOT, f"{title}.csv")
    with open(file_name, 'w') as f:
        f.write('\n')
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = f'attachment; filename={title}.csv'
    writer = csv.writer(response)
    header_list = []
    header_list_str = ''
    for i in schema_column:
        header_list.append(i.name)
        header_list_str += i.name
        header_list_str += schema.column_separator
    writer.writerow(header_list)
    with open(file_name, 'a') as f:
        f.write(header_list_str)
        f.write('\n')
    row_to_write = []
    str_to_write = ""
    for i in range(1, int(request.POST.get('data_rows')) + 1):
        for j in schema_column:
            if j.type == 'String':
                row_to_write.append(str_generate(schema.string_character))
            elif j.type == 'Boolean':
                row_to_write.append(boolean_generate())
            else:
                try:
                    row_to_write.append(int_generate(int(j.type_info1), int(j.type_info2)))
                except:
                    row_to_write.append(int_generate(0, 100))
        for g in row_to_write:
            str_to_write += g
            str_to_write += str(schema.column_separator)
        writer.writerow([str_to_write])
        with open(file_name, 'a') as f:
            f.write(str_to_write)
            f.write('\n')
        str_to_write = ''
        row_to_write.clear()
    return response
